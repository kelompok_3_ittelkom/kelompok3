<?php

namespace App\Http\Controllers;

use App\Pet;
use Illuminate\Http\Request;
use File;

class PetController extends Controller
{
    //
	public function index()
	{
		$peliharaans = Pet::all();
		return view('pet.index',['pets' => $peliharaans]);
	}


	public function create()
	{
		$data['module']['name'] = "Tambah Peliharaan";
		return view('pet.create',['data' => $data]);
	}


	public function store(Request $request)
	{
		$validateData = $request->validate([
			'kode' => 'required|size:3,unique:pets',
			'nama' => 'required|min:3|max:50',
			'jenis_kelamin' => 'required|in:B,J',
			'jenis' => 'required',
			'nama_majikan' => 'required',
			'image' => 'file|image|max:1000',
		]);
		$peliharaan = new Pet();
		$peliharaan->kode = $validateData['kode'];
		$peliharaan->name = $validateData['nama'];
		$peliharaan->gender = $validateData['jenis_kelamin'];
		$peliharaan->jenis = $validateData['jenis'];
		$peliharaan->name_owner = $validateData['nama_majikan'];
		if($request->hasFile('image'))
		{
			$extFile = $request->image->getClientOriginalExtension();
			$namaFile = 'user-'.time().".".$extFile;

			$path = $request->image->move('assets/images',$namaFile);
			$peliharaan->image = $path;
		}
		$peliharaan->save();
		$request->session()->flash('pesan','Penambahan data berhasil');
		return redirect()->route('pet.index');
	}

	public function edit($pet_id)
	{
		$result = Pet::findOrFail($pet_id);
		return view('pet.edit',['pet' => $result]);
	}

	public function update(Request $request, Pet $pet)
	{
		$validateData = $request->validate([
			'kode' => 'required|size:3,unique:pets',
			'nama' => 'required|min:3|max:50',
			'jenis_kelamin' => 'required|in:B,J',
			'jenis' => 'required',
			'nama_majikan' => 'required',
			'image' => 'file|image|max:1000',
		]);
		$pet->kode = $validateData['kode'];
		$pet->name = $validateData['nama'];
		$pet->gender = $validateData['jenis_kelamin'];
		$pet->jenis = $validateData['jenis'];
		$pet->name_owner = $validateData['nama_majikan'];
		if($request->hasFile('image'))
		{
			$extFile = $request->image->getClientOriginalExtension();
			$namaFile = 'user-'.time().".".$extFile;
			File::delete($pet->image);
			$path = $request->image->move('assets/images',$namaFile);
			$pet->image = $path;
		}
		$pet->save();
		$request->session()->flash('pesan','Perubahan data berhasil');
		return redirect()->route('pet.index',['Pet' => $pet->id]);
	}

	public function destroy(Request $request, Pet $pet)
	{
		File::delete($pet->image);
		$pet->delete();
		$request->session()->flash('pesan','Hapus data berhasil');
		return redirect()->route('pet.index');
	}
}
