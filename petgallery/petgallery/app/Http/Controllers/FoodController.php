<?php

namespace App\Http\Controllers;

use App\Food;
use Illuminate\Http\Request;
use File;

class FoodController extends Controller
{
    //
	public function index()
	{
		$makanans = Food::all();
		return view('makanan.food.index',['foods' => $makanans]);
	}


	public function create()
	{
		$data['module']['name'] = "Tambah Makanan";
		return view('makanan.food.create',['data' => $data]);
	}


	public function store(Request $request)
	{
		$validateData = $request->validate([
			'kode_makanan' => 'required|size:3,unique:foods',
			'nama_makanan' => 'required|min:3|max:50',
			'jenis_makanan' => 'required',
			'image' => 'file|image|max:1000',
		]);
		$makanan = new Food();
		$makanan->kode_food = $validateData['kode_makanan'];
		$makanan->name_food = $validateData['nama_makanan'];
		$makanan->jenis_food = $validateData['jenis_makanan'];
		if($request->hasFile('image'))
		{
			$extFile = $request->image->getClientOriginalExtension();
			$namaFile = 'user-'.time().".".$extFile;

			$path = $request->image->move('assets/images',$namaFile);
			$makanan->image = $path;
		}
		$makanan->save();
		$request->session()->flash('pesan','Penambahan data berhasil');
		return redirect()->route('makanan.food.index');
	}

	public function edit($food_id)
	{
		$result = Food::findOrFail($food_id);
		return view('makanan.food.edit',['food' => $result]);
	}

	public function update(Request $request, food $food)
	{
		$validateData = $request->validate([
			'kode_makanan' => 'required|size:3,unique:foods',
			'nama_makanan' => 'required|min:3|max:50',
			'jenis_makanan' => 'required',
			'image' => 'file|image|max:1000',
		]);
		$food->kode_food = $validateData['kode_makanan'];
		$food->name_food = $validateData['nama_makanan'];
		$food->jenis_food = $validateData['jenis_makanan'];
		if($request->hasFile('image'))
		{
			$extFile = $request->image->getClientOriginalExtension();
			$namaFile = 'user-'.time().".".$extFile;
			File::delete($food->image);
			$path = $request->image->move('assets/images',$namaFile);
			$food->image = $path;
		}
		$food->save();
		$request->session()->flash('pesan','Perubahan data berhasil');
		return redirect()->route('makanan.food.index',['food' => $food->id]);
	}

	public function destroy(Request $request, food $food)
	{
		File::delete($food->image);
		$food->delete();
		$request->session()->flash('pesan','Hapus data berhasil');
		return redirect()->route('makanan.food.index');
	}
}
