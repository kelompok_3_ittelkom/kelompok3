<?php

namespace App\Http\Controllers;

use Validator;
use App\Food;
use Illuminate\Http\Request;
use File;

class FoodApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $makanans = Food::all()->toJson(JSON_PRETTY_PRINT);
        return response($makanans, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'kode_makanan' => 'required|size:3,unique:food,kode',
            'nama_makanan' => 'required|min:3|max:50',
            'jenis_makanan' => 'required',
            'image' => 'required|file|image|max:1000',
            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
            } else {
                $makanan = new Food();
                $makanan->kode_food = $request->kode_makanan;
                $makanan->name_food = $request->nama_makanan;
                $makanan->jenis_food = $request->jenis_makanan;
                if($request->hasFile('image'))
                {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-'.time().".".$extFile;
                    $path = $request->image->move('assets/images',$namaFile);
                    $makanan->image = $path;
                }
                $makanan->save();
                return response()->json([
                "message" => "food record created"
                ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Food::where('id', $id)->exists()) {
            $validateData = Validator::make($request->all(), [
            'kode_makanan' => 'required|size:3,unique:food,kode',
            'nama_makanan' => 'required|min:3|max:50',
            'jenis_makanan' => 'required',
            'image' => 'required|file|image|max:1000',
            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
            } else {
                $makanan = Food::find($id);
                $makanan->kode_food = $request->kode_makanan;
                $makanan->name_food = $request->nama_makanan;
                $makanan->jenis_food = $request->jenis_makanan;
                if($request->hasFile('image'))
                {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-'.time().".".$extFile;
                    File::delete($makanan->image);
                    $path = $request->image->move('assets/images',$namaFile);
                    $makanan->image = $path;
                }
                $makanan->save();
                return response()->json([
                "message" => "food record updated"
                ], 201);
            }
        } else {
            return response()->json([
            "message" => "food not found"
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Food::where('id', $id)->exists()) {
            $makanan = Food::find($id);
            File::delete($makanan->image);
            $makanan->delete();
            return response()->json([
            "message" => "food record deleted"
            ], 201);
        } else {
            return response()->json([
            "message" => "food not found"
            ], 404);
        }
    }
}
