<?php

namespace App\Http\Controllers;

use Validator;
use App\Pet;
use Illuminate\Http\Request;
use File;

class PetApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pets = Pet::all()->toJson(JSON_PRETTY_PRINT);
        return response($pets, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'kode' => 'required|size:3,unique:pet,kode',
            'nama' => 'required|min:3|max:50',
            'jenis_kelamin' => 'required|in:B,J',
            'jenis' => 'required',
            'nama_majikan' => 'required',
            'image' => 'required|file|image|max:1000',
            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
            } else {
                $peliharaan = new Pet();
                $peliharaan->kode = $request->kode;
                $peliharaan->name = $request->nama;
                $peliharaan->gender = $request->jenis_kelamin;
                $peliharaan->jenis = $request->jenis;
                $peliharaan->name_owner = $request->nama_majikan;
                if($request->hasFile('image'))
                {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-'.time().".".$extFile;
                    $path = $request->image->move('assets/images',$namaFile);
                    $peliharaan->image = $path;
                }
                $peliharaan->save();
                return response()->json([
                "message" => "pet record created"
                ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Pet::where('id', $id)->exists()) {
            $validateData = Validator::make($request->all(), [
            'kode' => 'required|size:3,unique:pet,kode',
            'nama' => 'required|min:3|max:50',
            'jenis_kelamin' => 'required|in:B,J',
            'jenis' => 'required',
            'nama_majikan' => 'required',
            'image' => 'required|file|image|max:1000',
            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
            } else {
                $peliharaan = Pet::find($id);
                $peliharaan->kode = $request->kode;
                $peliharaan->name = $request->nama;
                $peliharaan->gender = $request->jenis_kelamin;
                $peliharaan->jenis = $request->jenis;
                $peliharaan->name_owner = $request->nama_majikan;
                if($request->hasFile('image'))
                {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-'.time().".".$extFile;
                    File::delete($peliharaan->image);
                    $path = $request->image->move('assets/images',$namaFile);
                    $peliharaan->image = $path;
                }
                $peliharaan->save();
                return response()->json([
                "message" => "Pet record updated"
                ], 201);
            }
        } else {
            return response()->json([
            "message" => "Pet not found"
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Pet::where('id', $id)->exists()) {
            $peliharaan = Pet::find($id);
            File::delete($peliharaan->image);
            $peliharaan->delete();
            return response()->json([
            "message" => "Pet record deleted"
            ], 201);
        } else {
            return response()->json([
            "message" => "Pet not found"
            ], 404);
        }
    }
}
