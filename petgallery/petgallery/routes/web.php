<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('lp');
});

Route::get('/','AdminPetController@landingpage')->name('lp');


Route::get('/login', 'AdminPetController@index')->name('login.index');
Route::get('/logout', 'AdminPetController@logout')->name('login.logout');
Route::post('/login', 'AdminPetController@process')->name('login.process');

Route::get('/index', 'PetController@index')->name('pet.index')->middleware('login_auth');
Route::get('/create', 'PetController@create')->name('pet.create')->middleware('login_auth');
Route::post('/pet', 'PetController@store')->name('pet.store')->middleware('login_auth');
Route::get('/pet/{pet}/edit', 'PetController@edit')->name('pet.edit')->middleware('login_auth');
Route::patch('/pet/{pet}', 'PetController@update')->name('pet.update')->middleware('login_auth');
Route::delete('/pet/{pet}', 'PetController@destroy')->name('pet.destroy')->middleware('login_auth');

Route::get('/food', 'FoodController@index')->name('makanan.food.index')->middleware('login_auth');
Route::get('/food/create', 'FoodController@create')->name('makanan.food.create')->middleware('login_auth');
Route::post('/food', 'FoodController@store')->name('makanan.food.store')->middleware('login_auth');
Route::get('/food/{food}/edit', 'FoodController@edit')->name('makanan.food.edit')->middleware('login_auth');
Route::patch('/food/{food}', 'FoodController@update')->name('makanan.food.update')->middleware('login_auth');
Route::delete('/food/{food}', 'FoodController@destroy')->name('makanan.food.destroy')->middleware('login_auth');
