<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('pet', 'PetApiController@store');
Route::get('pet', 'PetApiController@index');
Route::put('pet/{id}', 'PetApiController@update');
Route::delete('pet/{id}', 'PetApiController@destroy');

Route::post('food', 'FoodApiController@store');
Route::get('food', 'FoodApiController@index');
Route::put('food/{id}', 'FoodApiController@update');
Route::delete('food/{id}', 'FoodApiController@destroy');
