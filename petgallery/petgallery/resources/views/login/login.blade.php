@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('content')
<div class="right_col" role="main">
    <div>
        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    @if(session()->has('pesan'))
                    <div class="alert alert-success">
                        {{ session()->get('pesan') }}
                    </div>
                    @endif
                    <form action="{{ route('login.process') }}" method="POST">
                        @csrf
                        <h1>Login Form</h1>
                        <div>
                            @error('username')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <input type="text" class="form-control mr-5 @error ('username') is-invalid @enderror"
                                id="username" name="username" value="{{ old('username') }}" placeholder="Username" />
                        </div>
                        <div>
                            @error('password')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                id="password" name="password" value="{{ old('password') }}" placeholder="Password" />
                        </div>
                        <div>
                            <button type="submit" class="btn btn-default">Login</button>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">

                            <div class="clearfix"></div>
                            <br />

                            <div>
                                <h1><i class="fa fa-paw"></i> PET GALLERY</h1>
                                <p>©KELOMPOK 3. UAS PEMROGRAMAN WEB. IT TELKOM PURWOKERTO</p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>

        </div>
    </div>

</div>
@endsection
