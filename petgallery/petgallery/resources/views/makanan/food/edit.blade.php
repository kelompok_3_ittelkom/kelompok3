@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('content')
<div class="right_col" role="main">
    <div class="row">
        <div class="x_panel">
        <div class=" col-xl-6">
            <center><h1>Edit Food</h1></center>
            <hr>
            <form action="{{ route('makanan.food.update',['food' => $food->id]) }}" method="POST" enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="kode_makanan">kode_food</label>
                    <input type="text" class="form-control @error('kode_makanan') is-invalid @enderror" id="kode_makanan" name="kode_makanan"
                        value="{{ old('kode_makanan') ?? $food->kode_food }}">
                    @error('kode_makanan')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nama_makanan">Nama food</label>
                    <input type="text" class="form-control @error('nama_makanan') is-invalid @enderror" id="nama_makanan" name="nama_makanan"
                        value="{{ old('nama_makanan') ?? $food->name_food }}">
                    @error('nama_makanan')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="jenis">Spesies</label>
                    <select class="form-control" name="jenis_makanan" id="jenis_makanan">
                        <option value="Kucing" {{ (old('jenis_makanan') ?? $food->jenis_food)=='Kucing' ? 'selected': '' }}> Kucing
                        </option>
                        <option value="Anjing" {{ (old('jenis_makanan') ?? $food->jenis_food)=='Anjing' ? 'selected': '' }}> Anjing
                        </option>
                        <option value="Kelinci" {{ (old('jenis_makanan') ?? $food->jenis_food)=='Kelinci' ? 'selected': '' }}> Kelinci
                        </option>
                        <option value="Ikan" {{ (old('jenis_makanan') ?? $food->jenis_food)=='Ikan' ? 'selected': '' }}> Ikan
                        </option>
                        <option value="Burung" {{(old('jenis_makanan')??$food->jenis_food)=='Burung'?'selected':''}}> Burung
                        </option>
                    </select>
                    @error('jenis')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="image">Foto food</label>
                    <input type="file" class="form-control-file" id="image" name="image">
                    <br><img height="150px" src="{{url('')}}/{{$food->image}}" class="rounded" alt="">
                    @error('image')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary mb-2">Update</button>
            </form>
        </div>
    </div>
    </div>
</div>
@endsection
