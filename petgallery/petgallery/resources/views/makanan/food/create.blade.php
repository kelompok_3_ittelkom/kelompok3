@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('content')
<div class="right_col" role="main">
    <div class="row">
        <div class="x_panel">
        <div class=" col-xl-6">
            <center><h1>Tambah Makanan</h1></center>
            <hr>
            <form action="{{ route('makanan.food.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="kode_makanan">Serial Kode Makanan</label>
                    <input type="text" class="form-control @error('kode_makanan') is-invalid @enderror" id="kode_makanan" name="kode_makanan"
                        value="{{ old('kode_makanan') }}">
                    @error('kode_makanan')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nama_makanan">Nama Makanan</label>
                    <input type="text" class="form-control @error('nama_makanan') is-invalid @enderror" id="nama_makanan" name="nama_makanan"
                        value="{{ old('nama_makanan') }}">
                    @error('nama_makanan')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="jenis_makanan">Jenis Makanan</label>
                    <select class="form-control" name="jenis_makanan" id="jenis_makanan">
                        <option value="Kucing" {{ old('jenis_makanan')=='Kucing' ? 'selected': '' }}>
                            Kucing
                        </option>
                        <option value="Anjing" {{ old('jenis_makanan')=='Anjing' ? 'selected': '' }}>
                            Anjing
                        </option>
                        <option value="Kelinci" {{ old('jenis_makanan')=='Kelinci' ? 'selected': '' }}>
                            Kelinci
                        </option>
                        <option value="Ikan" {{ old('jenis_makanan')=='Ikan' ? 'selected': '' }}>
                            Ikan
                        </option>
                        <option value="Burung" {{ old('Burung')=='Burung' ? 'selected': '' }}>
                            Burung
                        </option>
                    </select>
                    @error('jenis_makanan')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="image">Foto Makanan</label>
                    <input type="file" class="form-control-file" id="image" name="image">
                    @error('image')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary mb-2">Tambah</button>
            </form>
        </div>
    </div>
    </div>
</div>
@endsection
