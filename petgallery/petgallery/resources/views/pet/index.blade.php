@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('content')
<div class="right_col" role="main">
    <div class="pull-right">
        <a href="{{ route('pet.create') }}" class="btn btn-primary">
            Tambah Pet
        </a>
        <a href="{{ route('login.logout') }}" class="btn btn-danger">
            Logout
        </a>
    </div>

    <div class="row">
        <div class="x_panel">
            <div class="col-12">
                <div class="py-4 d-flex justify-content-end align-items-center">
                    <h2 class="mr-auto">Tabel PET</h2>

                </div>
                @if(session()->has('pesan'))
                <div class="alert alert-success">
                    {{ session()->get('pesan') }}
                </div>
                @endif
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Foto</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Jenis</th>
                            <th>Nama Pemilik</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($pets as $peliharaan)
                        <tr>
                            <th>{{$loop->iteration}}</th>
                            <td><img height="50px" src="{{url('')}}/{{$peliharaan->image}}" class="rounded" alt=""></td>
                            <td>{{$peliharaan->kode}}</a></td>
                            <td>{{$peliharaan->name}}</td>
                            <td>{{$peliharaan->gender == 'B'?'Betina':'Jantan'}}</td>
                            <td>{{$peliharaan->jenis}}</td>
                            <td>{{$peliharaan->name_owner == '' ? 'N/A' : $peliharaan->name_owner}}</td>
                            <td>


                                <a href="{{ route('pet.edit',['pet' => $peliharaan->id]) }}">
                                    <button type="button" class="btn btn-primary btn-xs "><i
                                            class="fa fa-pencil"></i> Edit</button>
                                </a>
                                <form action="{{ route('pet.destroy',['pet'=>$peliharaan->id]) }}"
                                    method="POST">
                                   @method('DELETE')
                                   @csrf
                                   <button type="submit" class="btn btn-danger btn-xs"> <i
                                           class="fa fa-trash-o"></i> Hapus</button>
                               </form>

                            </td>
                        </tr>
                        @empty
                        <td colspan="8" class="text-center">Tidak ada data...</td>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
