@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('content')
<div class="right_col" role="main">
    <div class="row">
        <div class="x_panel">
        <div class=" col-xl-6">
            <center><h1>Tambah Pet</h1></center>
            <hr>
            <form action="{{ route('pet.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="kode">Kode</label>
                    <input type="text" class="form-control @error('kode') is-invalid @enderror" id="kode" name="kode"
                        value="{{ old('kode') }}">
                    @error('kode')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nama">Nama Pet</label>
                    <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama"
                        value="{{ old('nama') }}">
                    @error('nama')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="jantan" value="J"
                                {{ old('jenis_kelamin')=='J' ? 'checked': '' }}>
                            <label class="form-check-label" for="jantan">Jantan</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="betina" value="B"
                                {{ old('jenis_kelamin')=='B' ? 'checked': '' }}>
                            <label class="form-check-label" for="betina">Betina</label>
                        </div>
                        @error('jenis_kelamin')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="jenis">Spesies</label>
                    <select class="form-control" name="jenis" id="jenis">
                        <option value="Kucing" {{ old('jenis')=='Kucing' ? 'selected': '' }}>
                            Kucing
                        </option>
                        <option value="Anjing" {{ old('jenis')=='Anjing' ? 'selected': '' }}>
                            Anjing
                        </option>
                        <option value="Kelinci" {{ old('jenis')=='Kelinci' ? 'selected': '' }}>
                            Kelinci
                        </option>
                        <option value="Ikan" {{ old('jenis')=='Ikan' ? 'selected': '' }}>
                            Ikan
                        </option>
                        <option value="Burung" {{ old('Burung')=='Burung' ? 'selected': '' }}>
                            Burung
                        </option>
                    </select>
                    @error('jenis')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nama_majikan">Nama Majikan</label>
                    <input type="text" class="form-control @error('nama_majikan') is-invalid @enderror" id="nama_majikan" name="nama_majikan"
                        value="{{ old('nama_majikan') }}">
                    @error('nama_majikan')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="image">Foto Pet</label>
                    <input type="file" class="form-control-file" id="image" name="image">
                    @error('image')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary mb-2">Tambah</button>
            </form>
        </div>
    </div>
    </div>
</div>
@endsection
