<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>KELOMPOK 3 | PET GALLERY</title>

    <!-- Bootstrap -->
    <link href="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <!-- bootstrap-daterangepicker -->
    <link href="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ url('') }}/petgallery/vendor/polygon/gentelella/build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>PET GALLERY</span></a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="{{ url('') }}/petgallery/vendor/polygon/gentelella/production/images/favicon.ico" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>Admin</h2>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    @yield('leftbar')
                    <!-- /sidebar menu -->

                </div>
            </div>

            <!-- top navigation -->
            @yield('header')
            <!-- /top navigation -->

            <!-- page content -->
            @yield('content')
            <!-- /page content -->


        </div>
    </div>


    <!-- jQuery -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/Flot/jquery.flot.js"></script>
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/Flot/jquery.flot.time.js"></script>
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/moment/min/moment.min.js"></script>
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ url('') }}/petgallery/vendor/polygon/gentelella/build/js/custom.min.js"></script>

</body>

</html>
