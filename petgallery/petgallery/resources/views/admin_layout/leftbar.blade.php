<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('pet.index') }}">Pet</a></li>
                    <li><a href="{{ route('makanan.food.index') }}">Food</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('pet.create') }}">Tambah Pet</a></li>
                    <li><a href="{{ route('makanan.food.create') }}">Tambah Food</a></li>
                </ul>
            </li>
            <li><a href="{{ route('lp') }}"><i class="fa fa-laptop"></i> Landing Page <span
                        class="label label-success pull-right">Available</span></a></li>
        </ul>
    </div>
</div>
